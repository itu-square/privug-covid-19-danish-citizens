# COVID-19 Experiment using Figaro

This is the accompanying repository to the paper: _"How Attacker Knowledge Affects Privacy Risks: An Analysis Using Probabilistic Programming"_. The repository contains:

- The public datasets used in the paper (`data/`)
- The Figaro probabilistic models for analyses (`src/`)
	- The code in this repository builds on the Figaro probabilistic models in: https://bitbucket.org/itu-square/privug-experiments/
- The results of the analyses (`results/`)
- A Jupyter Notebook `ResultsNotebook.ipynb` with the plots presented in the paper (and additional plots not included in the paper).
	- The purpose of this notebook is to use the `matplotlib` library to produce high quality plots. The analyses are fully executed in Scala/Figaro. 


## Experiment Execution

We recommend running these experiments using the Simple Build Tool (SBT).

We use Figaro 5 which requires SBT version 0.13.16 or later. 
The documentation for SBT and installation guide is available at:
http://www.scala-sbt.org/0.13/tutorial/index.html.
