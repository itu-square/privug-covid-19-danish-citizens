// https://www.scala-sbt.org/1.x/docs/Howto-Project-Metadata.html

// _ATTRIBUTES_________________________________________________

// Following are used to name artifacts built in build process.
name    := "test"     // name of this project
version := "1.0"      // version of this project

// Set version of Scala.
scalaVersion := "2.12.10" // which version of Scala to use

// _DEPENDENCIES_______________________________________________

// _Figaro_____________________________________________________
// probabilistic programming library
libraryDependencies += "com.cra.figaro" %% "figaro" % "5.0.0.0"
// plotting library
libraryDependencies += "org.plotly-scala" %% "plotly-render" % "0.7.2"


// _RUN________________________________________________________

val mainClassName: String = "Governor" // Main class to run
mainClass in (Compile, run) := Some(mainClassName)
mainClass in assembly := Some(mainClassName)
