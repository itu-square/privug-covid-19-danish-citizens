import com.cra.figaro.language._
import com.cra.figaro.algorithm.sampling.Importance
import com.cra.figaro.library.compound.{If, _}
import com.cra.figaro.library.atomic.discrete._
import com.cra.figaro.library.collection.{ContainerElement, FixedSizeArrayElement, VariableSizeArray}
import plotly._

import scala.collection.mutable.ListBuffer
import scala.io.Source

object Governor {

  type Name = GroundData.Name
  type Zip = GroundData.ZIP
  type BDay = Int // to keep things simple for the time being
  type Age = Int

  sealed trait Sex
  case object Male extends Sex
  case object Female extends Sex


  sealed trait Diagnosis
  case object Healthy extends Diagnosis
  case object Ill extends Diagnosis


  // Custom class provided by Louise and Siv to hold "AgeInfo" from csv files
  case class AgeInfo(
                      Age: Int,
                      Population: Int,
                      AgeShare: Double
                    )

  val maleAgeDistribution: List[AgeInfo] = getAgeDistributionFromSex(Male)
  val femaleAgeDistribution: List[AgeInfo] = getAgeDistributionFromSex(Female)


  // Custom class provided by Louise and Siv to hold "IllInfo" from csv file
  case class IllInfo(
                      IntervalStart: Int,
                      IntervalEnd: Int,
                      FemaleShare: Double,
                      MaleShare: Double
                    )

  val illDistribution: List[IllInfo] = getIllDistribution


  // Distribute the age according to sex using real data about the Danish population
  def distributeAge(sex: Sex): Constant[Age] = {

    // We start by fetching the data set
    var ageDistribution = maleAgeDistribution
    if (sex == Female) ageDistribution = femaleAgeDistribution

    // Instead of the Flip function, we use a "roll" to have multiple clauses.

    // Making a roll that returns a double between 0-1
    val r = scala.util.Random
    val roll = r.nextDouble()

    // cumulativeTotal is introduced to avoid out-of-bounds exceptions:
    // In case there is an error in the data set and the sum of the total "AgeShare" isn't 1.
    // We avoid out of bounce errors by multiplying the roll with the cumulative total, thus getting a new modifiedRoll
    var cumulativeTotal = 0.00
    ageDistribution.foreach(i => cumulativeTotal += i.AgeShare)
    val modifiedRoll = roll * cumulativeTotal

    // Increases for each element of the list that we iterate through untill the modifiedRoll if-statement is triggered
    var cumulativeShare = 0.00

    // Age starts at -1 so we can localize errors later
    var age: Age = -1

    // Boolean to stop the while loop
    var ageFound = false

    var i = 0

    while (!ageFound) {

      // Prints an error message if the age has not been found.
      // We do this in the start of the loop to avoid out-of-bounds errors.
      if (i > ageDistribution.length) {
        println("ageDistribution: No age was found.")
        println("modifiedRoll: " + modifiedRoll + " / cumulativeShare: " + cumulativeShare + " / cumulativeTotal: " + cumulativeTotal)
        ageFound = true
      }

      cumulativeShare += ageDistribution(i).AgeShare

      if (modifiedRoll <= cumulativeShare) {
        age = ageDistribution(i).Age
        ageFound = true
      }

      i += 1
    }

    Constant(age)

  }

  def getAgeDistributionFromSex(sex: Sex): List[AgeInfo] = {
    var filePath = "data/ageMale.csv"
    if (sex != Male) {
      filePath = "data/ageFemale.csv"
    }

    def parse(fileLine: String): AgeInfo = {
      val ageInfo = fileLine.split(";")
      //println(fileLine+"   "+ageInfo)
      if (ageInfo.length < 3) {
        throw new Exception("Error loading the data with the expected number of columns" + ageInfo)
      } else {
        AgeInfo(ageInfo(0).toInt, ageInfo(1).toInt, ageInfo(2).toDouble)
      }
    }

    val inputFile = Source.fromFile(filePath, "UTF-8")
    val inputFileLines = inputFile.getLines().drop(1)
    val ageDistribution = ListBuffer.empty[AgeInfo]
    for (l <- inputFileLines) {
      val fileEntry = parse(l)

      ageDistribution.append(fileEntry)
    }
    inputFile.close()

    val ageDistributionList: List[AgeInfo] = ageDistribution.toList // Convert the ListBuffer it to a mutable list
    ageDistributionList
  }

  // Distribute the illness according to sex and age using real data about corona infected in the Danish population
  def distributeIllness(sex: Sex, age: Age): Element[Diagnosis]  = {

    var illnessShare = 0.0

    var illDistributed = false

    var i = 0

    while(!illDistributed){
      if(sex == Male){
        getIllDistribution.foreach(i => {
          if(age >= i.IntervalStart && age <= i.IntervalEnd) {
            illnessShare = i.MaleShare
            illDistributed = true
          }
        })
      } else {
        getIllDistribution.foreach(i => {
          if(age >= i.IntervalStart && age <= i.IntervalEnd) {
            illnessShare = i.FemaleShare
            illDistributed = true
          }
        })
      }
    }

    If(Flip(illnessShare), Ill, Healthy)
  }

  def getIllDistribution: List[IllInfo] = {
    var filePath = "data/coronaInfected.csv"

    def parse(fileLine: String): IllInfo = {
      val illInfo = fileLine.split(";")
      //println(fileLine+"   "+ageInfo)
      if (illInfo.length < 4) {
        throw new Exception("Error loading the data with the expected number of columns" + illInfo)
      } else {
        IllInfo(illInfo(0).toInt, illInfo(1).toInt, illInfo(2).toDouble, illInfo(3).toDouble)
      }
    }

    val inputFile = Source.fromFile(filePath, "UTF-8")
    val inputFileLines = inputFile.getLines().drop(1)
    val illDistribution = ListBuffer.empty[IllInfo]
    for (l <- inputFileLines) {
      val fileEntry = parse(l)

      illDistribution.append(fileEntry)
    }
    inputFile.close()

    val illDistributionList: List[IllInfo] = illDistribution.toList

    illDistributionList

  }

  // Probabilistic

  def main(args: Array[String]) = {

    // args format <NUM_SAMPLES: Int> <AGE_GOVERNOR: Int> <ANONYMIZA_AGE: Bool> <ANONYMIZA_ZIP: Bool> <ANONYMIZA_BDAY: Bool> <ENABLE_CONDITION: Bool> <NUM_AGES_CONDITION: Int> <COVID: Bool>
    if (args.size != 8) {
      //println("args format: <NUM_SAMPLES: Int> <AGE_GOVERNOR: Int> <ANONYMIZA_AGE: Bool> <ANONYMIZA_ZIP: Bool> <ANONYMIZA_BDAY: Bool> <ENABLE_CONDITION: Bool> <NUM_AGES_CONDITION: Int> <COVID: Bool>")
      throw new RuntimeException("args format: <NUM_SAMPLES: Int> <AGE_GOVERNOR: Int> <ANONYMIZA_AGE: Bool> <ANONYMIZA_ZIP: Bool> <ANONYMIZA_BDAY: Bool> <ENABLE_CONDITION: Bool> <NUM_AGES_CONDITION: Int> <COVID: Bool>")
    }


    val NUM_SAMPLES: Int          = args(0).toInt
    val AGE_GOVERNOR: Int         = args(1).toInt
    val ANONYMIZE_AGE: Boolean    = args(2).toBoolean
    val ANONYMIZE_ZIP: Boolean    = args(3).toBoolean
    val ANONYMIZE_BDAY: Boolean   = args(4).toBoolean
    val ENABLE_CONDITION: Boolean = args(5).toBoolean
    val NUM_AGES_CONDITION: Int   = args(6).toInt
    val COVID: Boolean            = args(7).toBoolean


    // Governor's info
    val GOVNAME: GroundData.Name   = "Governor"
    val GOVZIPCODE: GroundData.ZIP = 2300
    val GOVBDAY: BDay              = 44
    val GOVSEX: Sex                = Female
    val GOVAGE: Age                = AGE_GOVERNOR//90
    val GOVAGE_ARR: Array[Age]     = Array(10,30,50,70,90) // Array with an age per category
                                                           // val GOVAGE_ARR: Array[Age] = (1 to 112).toArray // Array of all ages
    val GOVDIAGNOSIS: Diagnosis    = Ill


    // To activate and deactive anonymization functions provided by Louise and Siv
    val zipAnonymizationActive: Boolean = ANONYMIZE_ZIP
    val bdayAnonymizationActive: Boolean = ANONYMIZE_BDAY
    val ageAnonymizationActive: Boolean = ANONYMIZE_AGE


    //********* Anonymizations provided by Louise and Siv *********//

    // Taking the value of z (zipcode) and changing it to a variable called zipBin dividing
    // zip codes into 4 bins
    def zipAnonymization(z: Zip): Zip = {
      var zipBin = z

      if(zipAnonymizationActive){
        zipBin = -1

        if (z >= 4000) {
          zipBin = 3
        } else if (z >= 3000) {
          zipBin = 2
        } else if (z >= 2000) {
          zipBin = 1
        } else {
          zipBin = 0
        }
      }

      zipBin
    }

    // Taking the value of b (BDay) and changing it to a variable called bdayBin dividing
    // BDay into 12 bins
    def bdayAnonymization(b: BDay): BDay = {
      var bdayBin = b

      if(bdayAnonymizationActive){
        bdayBin = -1

        if (b >= 330) {
          bdayBin = 11              // November 27. - Demcember 31.
        } else if (b >= 300) {
          bdayBin = 10              // Oktober 28. - November 26.
        } else if (b >= 270) {
          bdayBin = 9               // September 27. - Oktober 27.
        } else if (b >= 240) {
          bdayBin = 8               // August 28. - September 26.
        } else if (b >= 210) {
          bdayBin = 7               //July 29. - August 27.
        } else if (b >= 180) {
          bdayBin = 6               // June 29. - July 28.
        } else if (b >= 150) {
          bdayBin = 5               // May 30. - June 28.
        } else if (b >= 120) {
          bdayBin = 4               // April 30. - May 29.
        } else if (b >= 90) {
          bdayBin = 3               // March 31. - April 29.
        } else if (b >= 60) {
          bdayBin = 2               // March 1. - March 30.
        } else if (b >= 30) {
          bdayBin = 1               // January 31. - February 29.
        } else {
          bdayBin = 0               // January 1. - January 30.
        }
      }

      bdayBin
    }

    // Taking the value of a (age) and changing it to a variable called ageBin dividing
    // age into 5 bins
    def ageAnonymization(a: Age): Age = {
      var ageBin = a
      if(ageAnonymizationActive){
        ageBin = -1

        if (a >= 80) {
          ageBin = 4           // 80 years and above
        } else if (a >= 60) {
          ageBin = 3           // 60-79 years
        } else if (a >= 40) {
          ageBin = 2           // 40-59 years
        } else if (a >= 20) {
          ageBin = 1           // 20-39 years
        } else {
          ageBin = 0           // 0-19 years
        }
      }
      ageBin
    }


    object Probabilistic {

      // The anonymoization function, which removes names.
      // Activate the anonymizations for zip code, birthday and age by setting the values of
      // zipAnonymizationActive, bdayAnonymizationActive and ageAnonymizationActive to TRUE
      def alpha(records: FixedSizeArrayElement[(Name, Zip, BDay, Sex, Age, Diagnosis)]):
          ContainerElement[Int, (Zip, BDay, Sex, Age, Diagnosis)] =
        records map { case (n, z, b, s, a, d) => (zipAnonymization(z), bdayAnonymization(b), s, ageAnonymization(a), d) }

      // A prior
      def prior(): FixedSizeArrayElement[(Name, Zip, BDay, Sex, Age, Diagnosis)] = {

        // Database of constant size N
        val N: Int = 500
        val size: Element[Int] = Constant(N)

        // First row (i==0) includes governor's data
        // Remaining rows distributions modeling the assumptions of the datasets
        // We could remove the first row and add conditions (too slow for sampler)
        VariableSizeArray(size, i => for {
          n <- if (i == 0) Constant(GOVNAME)    else Uniform(GroundData.names: _*)
          z <- if (i == 0) Constant(GOVZIPCODE) else Uniform(GroundData.zips: _*)
          b <- if (i == 0) Constant(GOVBDAY)    else FromRange(0, 364)
          // COVID-19 prior
          s <- if (i == 0) Constant(GOVSEX)       else { if (!COVID) Uniform(Male, Female)    else  If(Flip(0.49),Male, Female) } // Sex distribution in Denmark
          a <- if (i == 0) Constant(GOVAGE)       else { if (!COVID) FromRange(0,112)         else  distributeAge(s)            } // Age distribution in Denmark
          d <- if (i == 0) Constant(GOVDIAGNOSIS) else { if (!COVID) If(Flip(.2),Ill,Healthy) else  distributeIllness(s, a)     } // Corona infected in Denmark
          // a <- /*if (i < 5) Constant(GOVAGE_ARR(i)) else*/ distributeAge(s) // Age distribution in Denmark
          // Uniform prior
          // s <- if (i == 0) Constant(GOVSEX)       else Uniform(Male, Female)
          // a <- if (i == 0) Constant(GOVAGE)       else FromRange(0,112)
          // d <- if (i == 0) Constant(GOVDIAGNOSIS) else If(Flip(.2), Ill, Healthy)
          // a <- if (i == 0) Constant(GOVAGE_ARR(i)) else FromRange(0,112)

        } yield (n, z, b, s, a, d))
      }
    }


    /*****************/
    /**** Queries ****/
    /*****************/
    val prior: FixedSizeArrayElement[(Name, Zip, BDay, Sex, Age, Diagnosis)] = Probabilistic.prior()
    val posterior: ContainerElement[Int, (Zip, BDay, Sex, Age, Diagnosis)] = Probabilistic.alpha(prior)


    //************Re-identification queries************//
    // P(∀ n ∈ (1,N) · zₙ = GOVZIPCODE → dₙ=Ill) where N is the size of the data set
    val governorIllZip: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (z == zipAnonymization(GOVZIPCODE)) d == Ill
        else d == Ill || d == Healthy // always true
    }
    val governorIllBday: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (b == bdayAnonymization(GOVBDAY)) d == Ill
        else true
    }
    val governorIllSex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (s == GOVSEX) d == Ill
        else true
    }
    val governorIllZipBday: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (z == zipAnonymization(GOVZIPCODE) && b == bdayAnonymization(GOVBDAY)) d == Ill
        else true
    }
    val governorIllZipSex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (z == zipAnonymization(GOVZIPCODE) && s == GOVSEX) d == Ill
        else true
    }
    val governorIllBdaySex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (b == bdayAnonymization(GOVBDAY) && s == GOVSEX) d == Ill
        else true
    }
    val governorIllZipBdaySex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (z == zipAnonymization(GOVZIPCODE) && b == bdayAnonymization(GOVBDAY) && s == GOVSEX) d == Ill
        else true
    }

    // Re-identification Queries with age provided by Louise and Siv
    val governorIllAge: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE)) d == Ill
        else true
    }
    val governorIllAgeZip: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && z == zipAnonymization(GOVZIPCODE)) d == Ill
        else true
    }
    val governorIllAgeBDay: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && b == bdayAnonymization(GOVBDAY)) d == Ill
        else true
    }
    val governorIllAgeSex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && s == GOVSEX) d == Ill
        else true
    }
    val governorIllAgeZipSex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && z == zipAnonymization(GOVZIPCODE) && s == GOVSEX) d == Ill
        else true
    }
    val governorIllAgeZipBDay: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && z == zipAnonymization(GOVZIPCODE) && b == bdayAnonymization(GOVBDAY)) d == Ill
        else true
    }
    val governorIllAgeBDaySex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && b == bdayAnonymization(GOVBDAY) && s == GOVSEX) d == Ill
        else true
    }
    val governorIllAgeZipBDaySex: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a == ageAnonymization(GOVAGE) && z == zipAnonymization(GOVZIPCODE) && b == bdayAnonymization(GOVBDAY) && s == GOVSEX) d == Ill
        else true
    }

    // Re-identification Queries without Governor provided by Louise and Siv
    val recordsAgeNinetyAndAboveIll: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (d == Ill) a >= ageAnonymization(90) // age 90 ageGroup 4
        else true
    }
    val recordsIllAgeNinetyAndAbove: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(90)) d == Ill // age 90 ageGroup 4
        else true
    }
    val recordsIllAgeIntervalThirty: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(20) && a < ageAnonymization(30)) d == Ill // age 30 && 40 ageGroup 1 && 2
        else true
    }
    val recordsIllAgeNinetyAndAboveFemale: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(90) && s == Female) d == Ill // age 90 ageGroup 4
        else true
    }
    val recordsIllAgeNinetyAndAboveMale: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(90) && s == Male) d == Ill // age 90 ageGroup 4
        else true
    }
    val recordsIllAgeIntervalThirtyFemale: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Female) d == Ill // // age 30 && 40 ageGroup 1 && 2
        else true
    }
    val recordsIllAgeIntervalThirtyMale: Element[Boolean] = posterior forall {
      case (z, b, s, a, d) =>
        if (a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Male) d == Ill // // age 30 && 40 ageGroup 1 && 2
        else true
    }


    //*********Sanity checks*********//
    val governorIllPrior: Element[Boolean] = prior exists {
      case (n, z, b, s, a, d) =>
        n == GOVNAME &&
          d == Ill
    }
    val firstRowDiagnosis: Element[Diagnosis] = posterior(0)._5


    //*********Accuracy checks*********//
    val secondRowZip:  Element[Int]   = posterior(1)._1
    val secondRowBDay: Element[Int]   = posterior(1)._2
    val secondRowAge:  Element[Int]   = posterior(1)._4


    //*********Attributes uniqueness queries******//
    val numGovZip: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE)
    }

    val numGovBday: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        b == bdayAnonymization(GOVBDAY)
    }
    val numGovSex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        s == GOVSEX
    }
    val numGovZipBday: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          b == bdayAnonymization(GOVBDAY)
    }
    val numGovZipSex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          s == GOVSEX
    }
    val numGovBdaySex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        s == GOVSEX &&
          b == bdayAnonymization(GOVBDAY)
    }
    val numGovZipBdaySex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          b == bdayAnonymization(GOVBDAY) &&
          s == GOVSEX
    }

    // Attribute Uniqueness Queries provided by Louise and Siv
    val numGovAge: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a == ageAnonymization(GOVAGE)
    }


    // ******** NEW QUERIES ******** //

    // val numGovAge10: Element[Int] = posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE_ARR(0))
    // }

    // val numGovAge30: Element[Int] = posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE_ARR(1))
    // }

    // val numGovAge50: Element[Int] = posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE_ARR(2))
    // }

    // val numGovAge70: Element[Int] = posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE_ARR(3))
    // }

    // val numGovAge90: Element[Int] = posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE_ARR(4))
    // }

    // def query(i: Int): Element[Int] = {
    //   posterior count {
    //     case (z, b, s, a, d) =>
    //       a == ageAnonymization(GOVAGE_ARR(i))
    //   }
    // }

    // val queries: Array[Element[Int]] = GOVAGE_ARR.zipWithIndex.map { case (i: Int, idx: Int) => query(idx)}
    // ******** NEW QUERIES ******** //


    // Attribute Uniqueness Queries without the Governor provided by Louise and Siv
    val numRecordsAgeAboveThirty: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a > ageAnonymization(30)
    }
    val numRecordsAgeNinetyAndAbove: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90)
    }
    val numRecordsIntervalThirty: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(40)
    }
    val numRecordsAgeNinetyAndAboveFemale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90) && s == Female
    }
    val numRecordsAgeNinetyAndAboveMale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90) && s == Male
    }
    val numRecordsIntervalThirtyFemale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Female
    }
    val numRecordsIntervalThirtyMale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Male
    }

    //*********Probability of learning diagnosis queries******//
    val numGovIllZip: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          d == Ill
    }
    val numGovIllBday: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        b == bdayAnonymization(GOVBDAY) &&
          d == Ill
    }
    val numGovIllSex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        s == GOVSEX &&
          d == Ill
    }
    val numGovIllZipBday: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          b == bdayAnonymization(GOVBDAY) &&
          d == Ill
    }
    val numGovIllZipSex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          s == GOVSEX &&
          d == Ill
    }
    val numGovIllBdaySex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        s == GOVSEX &&
          b == bdayAnonymization(GOVBDAY) &&
          d == Ill
    }
    val numGovIllZipBdaySex: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        z == zipAnonymization(GOVZIPCODE) &&
          b == bdayAnonymization(GOVBDAY) &&
          s == GOVSEX &&
          d == Ill
    }

    // Probability of Learning Diangnosis Queries provided by Louise and Siv
    val numGovIllAge: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a == ageAnonymization(GOVAGE) &&
          d == Ill
    }

    // Probability of Learning Diangnosis Queries without the Governor provided by Louise and Siv
    val numRecordsIllAgeNinetyAndAbove: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90) &&
          d == Ill
    }
    val numRecordsIllIntervalThirty: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(30) &&
          d == Ill
    }
    val numRecordsIllAgeNinetyAndAboveFemale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90) && s == Female &&
          d == Ill
    }
    val numRecordsIllAgeNinetyAndAboveMale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(90) && s == Male &&
          d == Ill
    }
    val numRecordsIllIntervalThirtyFemale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Female &&
          d == Ill
    }
    val numRecordsIllIntervalThirtyMale: Element[Int] = posterior count {
      case (z, b, s, a, d) =>
        a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Male &&
          d == Ill
    }


        //*********Conditioning*********//

   /* posterior count {
        case (z, b, s, a, d) =>
          z == zipAnonymization(GOVZIPCODE)
    } setCondition ((x: Int) => x == ?) // The "?" should be the number of people with the Governor's zip code
   */

    // Conditioning on age provided by Louise and Siv
    // *** This is the condition to activate if necessary *** //
    // posterior count {
    //   case (z, b, s, a, d) =>
    //     a == ageAnonymization(GOVAGE)
    // } setCondition ((x: Int) => x == 24) // The "?" should be the number of people with the Governor's age
    if (ENABLE_CONDITION) {
      posterior count {
        case (z, b, s, a, d) =>
          a == ageAnonymization(GOVAGE)
      } setCondition ((x: Int) => x == NUM_AGES_CONDITION) // The "?" should be the number of people with the Governor's age
    }


    // Conditioning on age without the Governor provided by Louise and Siv

    /* posterior count {
          case (z, b, s, a, d) =>
            a >= ageAnonymization(90)
         } setCondition ((x: Int) => x == ?) // The "?" should be the number of people with an age above ninety
    */
/*
      posterior count {
          case (z, b, s, a, d) =>
            a >= ageAnonymization(90) && s == Female
         } setCondition ((x: Int) => x == 4) // The "?" should be the number of people with an age above ninety and female
*/

    /*   posterior count {
          case (z, b, s, a, d) =>
            a >= ageAnonymization(90) && s == Male
         } setCondition ((x: Int) => x == ?) // The "?" should be the number of people with an age above ninety and female
    */

    /*   posterior count {
          case (z, b, s, a, d) =>
            a >= ageAnonymization(20) && a < ageAnonymization(30)
          } setCondition ((x: Int) => x == ?) // The "?" should be the number of people with an age in interval 30 - 39
    */

    /*   posterior count {
          case (z, b, s, a, d) =>
            a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Female
          } setCondition ((x: Int) => x == ?) // The "?" should be the number of females with an age in interval 30 - 39
    */

    /*    posterior count {
            case (z, b, s, a, d) =>
             a >= ageAnonymization(20) && a < ageAnonymization(30) && s == Male
           } setCondition ((x: Int) => x == ?) // The "?" should be the number of males with an age in interval 30 - 39
    */


        val alg = Importance(NUM_SAMPLES,//2,

          // // Sanity Checks
          // governorIllPrior, firstRowDiagnosis,

          // //Re-identification queries
          // governorIllZip, governorIllBday, governorIllSex,
          // governorIllZipSex, governorIllZipBday, governorIllBdaySex,
          // governorIllZipBdaySex,

          //Provided by Louise and Siv
          governorIllAge, governorIllAgeZip, governorIllAgeBDay,
          governorIllAgeSex, governorIllAgeZipBDay, governorIllAgeZipSex, governorIllAgeBDaySex, governorIllAgeZipBDaySex,

          // recordsAgeNinetyAndAboveIll, recordsIllAgeNinetyAndAbove, recordsIllAgeIntervalThirty,
          // recordsIllAgeNinetyAndAboveFemale, recordsIllAgeNinetyAndAboveMale,
          // recordsIllAgeIntervalThirtyFemale, recordsIllAgeIntervalThirtyMale,

          //Attribute uniqueness queries
          // numGovZip, numGovBday, numGovSex,
          // numGovZipSex, numGovZipBday, numGovBdaySex,
          // numGovZipBdaySex,

          // // Provided by Louise and Siv
          numGovAge,

          // numRecordsAgeAboveThirty, numRecordsAgeNinetyAndAbove, numRecordsIntervalThirty,
          // numRecordsAgeNinetyAndAboveFemale, numRecordsAgeNinetyAndAboveMale,
          // numRecordsIntervalThirtyFemale, numRecordsIntervalThirtyMale,

          //Probability of learning diagnosis queries
          // numGovIllZip, numGovIllBday, numGovIllSex,
          // numGovIllZipSex, numGovIllZipBday, numGovIllBdaySex,
          // numGovIllZipBdaySex,

          // //Accuracy
          // secondRowBDay,secondRowAge,

          //New queries
          // numGovAge10,numGovAge30,numGovAge50,numGovAge70,numGovAge90,
          // queries:_*

          // // Provided by Louise and Siv
          numGovIllAge,
          // numRecordsIllAgeNinetyAndAbove, numRecordsIllIntervalThirty,

          // numRecordsIllAgeNinetyAndAboveFemale, numRecordsIllAgeNinetyAndAboveMale,
          // numRecordsIllIntervalThirtyFemale, numRecordsIllIntervalThirtyMale
        )
    alg.start()

    // PlotFigaro.plotElementIntProbabilities(alg,secondRowBDay,
    //   1, (0,365),"Prior BDay", "secondRowBday.html")

    // PlotFigaro.plotElementIntProbabilities(alg,secondRowAge,
    //   1, (0,200),"Prior Age", "secondRowAge.html")

    //     println("\n\n//************Sanity checks************//")
    //     println("governor illness prior: " +
    //       alg.probability(governorIllPrior, (x: Boolean) => x))
    //     println("Element in first row (governor) illness: " +
    //       alg.probability(firstRowDiagnosis, (x: Diagnosis) => x == Ill))


    //     println("\n\n//************Re-identification queries************//")
    //     println("1. P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE →  dₙ=Ill)                               = " +
    //       alg.probability(governorIllZip, (x: Boolean) => x))
    //     println("2. P(∀ n ∈  (1,N) · bₙ = GOVBDAY    →  dₙ=Ill)                               = " +
    //       alg.probability(governorIllBday, (x: Boolean) => x))
    //     println("3. P(∀ n ∈  (1,N) · sₙ = GOVSEX     →  dₙ=Ill)                               = " +
    //       alg.probability(governorIllSex, (x: Boolean) => x))
    //     println("4. P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY  →  dₙ=Ill)               = " +
    //       alg.probability(governorIllZipBday, (x: Boolean) => x))
    //     println("5. P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
    //       alg.probability(governorIllZipSex, (x: Boolean) => x))
    //     println("6. P(∀ n ∈  (1,N) · bₙ = GOVBDAY    ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
    //       alg.probability(governorIllBdaySex, (x: Boolean) => x))
    //     println("7. P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY ∧ sₙ = GOVSEX  →  dₙ=Ill) = " +
    //       alg.probability(governorIllZipBdaySex, (x: Boolean) => x))

    // Re-identification Queries provided by Louise and Siv
        println("\n\n//************Louise & Siv's queries with age************//")
        println("8. P(∀ n ∈  (1,N) · aₙ = GOVAGE →  dₙ=Ill)                                    = " +
          alg.probability(governorIllAge, (x: Boolean) => x))
        println("9. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ zₙ = GOVZIPCODE  →  dₙ=Ill)                 = " +
          alg.probability(governorIllAgeZip, (x: Boolean) => x))
        println("10. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ bₙ = GOVBDAY  →  dₙ=Ill)                    = " +
          alg.probability(governorIllAgeBDay, (x: Boolean) => x))
        println("11. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ bₙ = GOVSEX  →  dₙ=Ill)                      = " +
          alg.probability(governorIllAgeSex, (x: Boolean) => x))
        println("12. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY  →  dₙ=Ill)   = " +
          alg.probability(governorIllAgeZipBDay, (x: Boolean) => x))
        println("13. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ zₙ = GOVZIPCODE ∧ sₙ = GOVSEX  →  dₙ=Ill)   = " +
           alg.probability(governorIllAgeZipSex, (x: Boolean) => x))
        println("14. P(∀ n ∈  (1,N) · aₙ = GOVAGE ∧ zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY ∧ sₙ = GOVSEX  →  dₙ=Ill)   = " +
            alg.probability(governorIllAgeZipBDaySex, (x: Boolean) => x))

    //     println("15. P(∀ n ∈  (1,N) · dₙ=Ill → aₙ >= 90 )                                          = " +
    //       alg.probability(recordsAgeNinetyAndAboveIll, (x: Boolean) => x))
    //     println("16. P(∀ n ∈  (1,N) · aₙ >= 90  → dₙ=Ill)                                          = " +
    //       alg.probability(recordsIllAgeNinetyAndAbove, (x: Boolean) => x))
    //     println("17. P(∀ n ∈  (1,N) · aₙ >= 30 && aₙ < 40  → dₙ=Ill)                               = " +
    //       alg.probability(recordsIllAgeIntervalThirty, (x: Boolean) => x))
    //     println("18. P(∀ n ∈  (1,N) · aₙ >= 90 ∧ sₙ = Female → dₙ=Ill)                                          = " +
    //       alg.probability(recordsIllAgeNinetyAndAboveFemale, (x: Boolean) => x))
    //     println("19. P(∀ n ∈  (1,N) · aₙ >= 90 ∧ sₙ = Male → dₙ=Ill)                                          = " +
    //      alg.probability(recordsIllAgeNinetyAndAboveMale, (x: Boolean) => x))
    //     println("20. P(∀ n ∈  (1,N) · aₙ >= 30 && aₙ < 40 ∧ sₙ = Female  → dₙ=Ill)                               = " +
    //       alg.probability(recordsIllAgeIntervalThirtyFemale, (x: Boolean) => x))
    //     println("21. P(∀ n ∈  (1,N) · aₙ >= 30 && aₙ < 40 ∧ sₙ = Male  → dₙ=Ill)                               = " +
    //       alg.probability(recordsIllAgeIntervalThirtyMale, (x: Boolean) => x))

    //     println("\n\n//************Attribute uniqueness Plots************//")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovZip,
    //       1, (0, 1000), //0, 15
    //       "1) Number of rows with Governor's Zip",
    //       "numgovzip.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovBday,
    //       1, (0, 1000), // 0, 10
    //       "2) Number of rows with Governor's BDay",
    //       "numgovbday.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovSex,
    //       1, (0, 1000), // 450, 550
    //       "3) Number of rows with Governor's Sex",
    //       "numgovsex.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovZipSex,
    //       1, (0, 1000), //0, 10
    //       "4) Number of rows with Governor's Zip and Sex",
    //       "numgovzipsex.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovZipBday,
    //       1, (0, 1000), // 0, 10
    //       "5) Number of rows with Governor's Zip and BDay",
    //       "numgovzipbday.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovBdaySex,
    //       1, (0, 1000), //10, 70
    //       "6) Number of rows with Governor's BDay & Sex",
    //       "numgovbdaysex.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovZipBdaySex,
    //       1, (0, 1000), // 0,10
    //       "7) Number of rows with Governor's Zip & BDay & Sex",
    //       "numgovzipbdaysex.html")


    // // Attribute uniqueness plots using different ages
    // GOVAGE_ARR.zipWithIndex.map {
    //   case (i: Int, idx: Int) => PlotFigaro.plotElementIntProbabilities(alg, queries(idx), 1, (0, 500),
    //                                                                     "Number of rows with Representative's Age = "+i, "numrepage"+i+"_new.html")
    // }

    // PlotFigaro.plotElementIntProbabilities(alg, queries(1), 1, (0, 500), " Number of rows with Representative's Age = 10", "numrepage10.html")
    // PlotFigaro.plotElementIntProbabilities(alg, numGovAge30, 1, (0, 500), " Number of rows with Representative's Age = 30", "numrepage30.html")
    // PlotFigaro.plotElementIntProbabilities(alg, numGovAge50, 1, (0, 500), " Number of rows with Representative's Age = 50", "numrepage50.html")
    // PlotFigaro.plotElementIntProbabilities(alg, numGovAge70, 1, (0, 500), " Number of rows with Representative's Age = 70", "numrepage70.html")
    // PlotFigaro.plotElementIntProbabilities(alg, numGovAge90, 1, (0, 500), " Number of rows with Representative's Age = 90", "numrepage90.html")


    // Attribute Uniqueness Plots provided by Louise and Siv
        PlotFigaro.plotElementIntProbabilities(alg, numGovAge,
          1, (0, 500), // 80, 170 *** This is the one for Figure 8 in the paper ***
          "8) Number of rows with Governor's Age",
          "numgovage.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsAgeAboveThirty,
    //       1, (0, 1000), //680, 850
    //       "9) Number of rows above thirty",
    //       "numageabovethirty.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsAgeNinetyAndAbove,
    //       1, (0, 1000), //600, 800
    //       "10) Number of rows above ninety",
    //       "numageaboveninety.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIntervalThirty,
    //       1, (0, 1000), // 600, 800
    //       "11) Number of rows in age interval 20 - 30 ",
    //       "numageintervalthirty.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsAgeNinetyAndAboveFemale,
    //       1, (0, 1000), //70, 200
    //       "12) Number of rows above ninety and are female",
    //       "numageaboveninetyfemale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsAgeNinetyAndAboveMale,
    //       1, (0, 1000), // 70, 200
    //       "13) Number of rows above ninety and are male",
    //       "numageaboveninetymale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIntervalThirtyFemale,
    //       1, (0, 1000), //40, 70
    //       "14) Number of rows in age interval 30 - 40 and are female",
    //       "numageintervalthirtyfemale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIntervalThirtyMale,
    //       1, (0, 1000), // 40, 70
    //       "15) Number of rows in age interval 30 - 40 and are male",
    //       "numageintervalthirtymale.html")


    //     println("\n\n//************Probability of Learning Diagnosis Plots************//")

    //     PlotFigaro.plotElementIntProbabilities(alg, numGovIllZip,
    //       1, (0, 1000), //0, 10
    //       "16) Number of rows with Governor's Zip and are Ill",
    //       "numgovillzip.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numGovIllZipBdaySex,
    //       1, (0, 1000), // 0, 10
    //       "17) Number of rows with Governor's Zip & BDay & Sex and are Ill",
    //       "numgovillzipbdaysex.html")

    // Probability of Learning Diagnosis Plots provided by Louise and Siv
        PlotFigaro.plotElementIntProbabilities(alg, numGovIllAge,
          1, (0, 500), // 0, 10 *** This is the one for Figure 9 in the paper ***
          "18) Number of rows with governors Age and are Ill",
          "numgovillage.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllAgeNinetyAndAbove,
    //       1, (0, 1000), // 45, 50
    //       "19) Number of rows above ninety and are Ill",
    //       "numillaboveninetyage.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllIntervalThirty,
    //       1, (0, 1000), //0, 35
    //       "20) Number of rows in age interval 30 - 40 and are Ill",
    //       "numillageintervalthirty.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllAgeNinetyAndAboveFemale,
    //       1, (0, 1000),
    //       "21) Number of rows above ninety, female and are Ill",
    //       "numillaboveninetyagefemale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllAgeNinetyAndAboveMale,
    //       1, (0, 1000),
    //       "22) Number of rows above ninety, male and are Ill",
    //       "numillaboveninetyagemale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllIntervalThirtyFemale,
    //       1, (0, 1000),
    //       "23) Number of rows in age interval 30 - 40, female and are Ill",
    //       "numillageintervalthirtyfemale.html")
    //     PlotFigaro.plotElementIntProbabilities(alg, numRecordsIllIntervalThirtyMale,
    //       1, (0, 1000),
    //       "24) Number of rows in age interval 30 - 40, male and are ill",
    //       "numillageintervalthirtymale.html")

    /*    // X-axis
        val x = (0 to 225)

        // Y-axis (Probabilities)
        val y = x.map { (i) => alg.probability(numGovIllZip, (x: Int) => i == x) }

        // Plotting
        // Define plot
        val plot = Bar(
          x.map((x) => x * 1.0 / 225.0),
          y
        )
        val data = Seq(plot)

        //Layout
        val layout = Layout(
          title = "25) Number of rows with Governor's Zip and are Ill"
        )
        // Show plot
        Plotly.plot(
          "test.html",
          data,
          layout
        )
    */

   /* // X-axis
        val x = (0 to 10)

    // Y-axis (Probabilities)
         val y = x.map { (i) => alg.probability(numGovIllAge, (x: Int) => i == x) }

    // Plotting
    // Define plot
         val plot = Bar(
            x.map((x) => x * 1.0 / 10.0),
      y
    )
      val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = "26) Number of rows with Governor's age and are Ill"
    )
    // Show plot
    Plotly.plot(
      "test2.html",
      data,
      layout
    )
    */

        alg.kill()
      }
    }
