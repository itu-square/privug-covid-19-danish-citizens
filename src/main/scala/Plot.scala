import plotly._, element._, layout._, Plotly._
import com.cra.figaro.algorithm.sampling.Importance
import com.cra.figaro.library.atomic.continuous.KernelDensity
import com.cra.figaro.language.Element

object PlotFigaro {
  /******************************/
  /*** Plotting Probabilities ***/
  /******************************/

  /**
    * A method to abstract away the details of plotting
    * 
    * @param alg The sampling algorithm to use (for now only
    * `Importance`)
    * @param dist The distribution to plot
    * @param precision The precision of the plotting, e.g., 10 plots
    * with one decimal 0.1, 0.2, 0.3, ...
    * @param range A pair indicating the first and last element of the
    * x-axis
    * @param title Title of the plot
    * @param filename filename of the generated html file
    */
  def plotElementDoubleProbabilities(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int),
    title: String, filename: String) {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Double) => i <= x && x < i + delta)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )
  }

  def plotElementIntProbabilities(alg: Importance, dist: Element[Int], precision: Int, range: (Int, Int),
    title: String, filename: String) {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Int) => i <= x && x < i + delta)}

    // Plotting
    // Define plot
    val plot = Bar(
      x,
      y
    )    
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )

  }

  def computeXY(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int)):
      (IndexedSeq[Double], IndexedSeq[Double]) = {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Double) => i < x && x <= i + delta)}

    return (x,y)
  }


  /********************/
  /*** Plotting kde ***/
  /********************/
  def plotKDE(alg: Importance, dist: Element[Double],
    precision: Int, range: (Int, Int),
    title: String, filename: String) {

    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val samples = alg.computeDistribution(dist)
    val samples_prime = samples map {_._2}

    // Compute bandwith (as described in
    // https://en.wikipedia.org/wiki/Kernel_density_estimation#A_rule-of-thumb_bandwidth_estimator)
    val sd: Double = Math.sqrt(alg.variance(dist))
    val bandwith: Double = Math.pow((4*Math.pow(sd, 5))/(3*samples_prime.size), 0.2)  // 1/5

    val kde = KernelDensity(samples_prime, bandwith)
    val y = x.map{ (i) => kde.density(i)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )
  }

  def computeXYKDE(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int)):
      (IndexedSeq[Double], IndexedSeq[Double]) = {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis
    val samples = alg.computeDistribution(dist)
    val samples_prime = samples map {_._2}

    // Compute bandwith (as described in
    // https://en.wikipedia.org/wiki/Kernel_density_estimation#A_rule-of-thumb_bandwidth_estimator)
    val sd: Double = Math.sqrt(alg.variance(dist))
    val bandwith: Double = Math.pow((4*Math.pow(sd, 5))/(3*samples_prime.size), 0.2)  // 1/5

    val kde = KernelDensity(samples_prime, bandwith)
    val y = x.map{ (i) => kde.density(i)}

    return (x,y)
  }
}
